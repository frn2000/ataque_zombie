# Ataque Zombie

En Ataque Zombie deberás usar bombas para liberarte de los zombies que te persiguen, o correr tan rápido como puedas a la puerta que se encuentra en la parte superior de cada nivel.

![atl text](images/screenshot.png)

## ¿Cómo compilar?

Este software requiere un compilador de c++ que soporte C++11 y la librería ncurses (versión 6).

## Licencia

El zombie hecho en arte ASCII fue diseñado por Joris Bellenger y modificado por nosotros.
Para ver el original, dirigirse a la [galería del autor.](https://svzanten.home.xs4all.nl/ascii/line_art/)

El resto del presente software se encuetra bajo la GNU General Public License version 3 o superior ([GPL 3.0+](http://www.gnu.org/licenses/gpl-3.0.en.html)). 
El texto completo puede leerse en el archivo [LICENSE](LICENSE)