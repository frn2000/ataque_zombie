/* Copyright 2017 Di Pietro M. and Purrello V.

 This file is part of Ataque Zombie.

 Ataque Zombie is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Ataque Zombie is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Ataque Zombie.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _WORLD_H
#define _WORLD_H

#include <iostream>
#include <ctime>
#include <string>
#include <vector>
#include "window.h"

#define ALIVE 0
#define EATEN 1
#define BOMBED 2

#define GAMING 0
#define LVLUP 1
#define LOST 2
#define QUIT 3

#define LOCK 0
#define OPEN 1

// Secret state
#define HIDDEN 0
#define FOUND 1

// Secret type
#define LASTING_CLOCK 0
#define LARGER_BOMB 1
#define TALKING_SECRET 2

// Zombie type
#define FOLLOWER 0
#define RANDWALK 1

#define FOLLOWERS_POP 0.5

using namespace std;

class World
{
    public:
        World (char hchar, int lvl, int current_score)
        {
            srand ((unsigned) time(0));
            length[0] = (COLS - OFFSET*2); // LENGTH_X
            length[1] = (LINES - OFFSET*2); // LENGTH_Y

            set_game_state (GAMING);
            play_again = true;
            set_level (lvl);
            set_score (current_score);

            init_doors ();

            hero_char = hchar;
            hero[0] = length[0]/2;
            hero[1] = length[1]/2;
            set_hero_state (ALIVE);

            max_bomb_clock = 3;
            bomb_clock = -1;
            // Initialize to avoid compiler warnings
            bomb[0] = 0;
            bomb[1] = 0;

            init_secret ();

            init_zombies ();
        }

        void print_map ()
        {
            map_win = create_win (length[1], length[0],
                    (COLS-length[0])/2, (LINES-length[1])/2);
            mvwprintw (map_win, door[1], door[0], "#");
            mvwprintw (map_win, hero[1], hero[0], "%c", hero_char);
            for (auto& s: secret) {
                if (s.at(2) == FOUND){
                    if (s.at(3) == LASTING_CLOCK)
                        mvwprintw (map_win, s.at(1), s.at(0), "i");
                    else
                        mvwprintw (map_win, s.at(1), s.at(0), "Y");
                }
            }
            for (auto z: zombie) {
                mvwprintw (map_win, z.at(1), z.at(0), "&");
            }
            if (bomb_clock == 0)
                mvwprintw (map_win, bomb[1], bomb[0], "*");
            else if (bomb_clock > 0)
// #ifdef ONLY_ASCII
                mvwprintw (map_win, bomb[1], bomb[0], "o");
// #else
//                 mvwprintw (map_win, bomb[1], bomb[0], "ó");
// #endif /* ONLY_ASCII */

            wrefresh (map_win);
            destroy_win (map_win);
            refresh_upper_panel ();
            refresh_bottom_panel ();
            refresh_foot_text ();
        }

        void move (int dx, int dy)
        {
            if ((hero[0] + dx) == door[0] && (hero[1] + dy) == door[1] && door[2] == OPEN){
                hero[0] += dx;
                hero[1] += dy;
                set_game_state (LVLUP);
                return;
            }
            if ((hero[0] + dx) == 0
                    || (hero[0] + dx) == (length[0] - 1)
                    || (hero[1] + dy) == 0
                    || (hero[1] + dy) == (length[1] - 1))
                return;

            hero[0] += dx;
            hero[1] += dy;

            for (auto& s: secret) {
                if (hero[0] == s.at(0) && hero[1] == s.at(1)) {
                    if (s.at(2) != FOUND) {
                        if (s.at(3) == LASTING_CLOCK){
                            max_bomb_clock += 1;
                            show_centered_msg (STR_BOMB_TIME);
                        }
                        else{
                            const vector<string> talk_msg = {
                                STR_TALKING_SECRET1,
                                STR_TALKING_SECRET2,
                                STR_TALKING_SECRET3,
                                STR_TALKING_SECRET4,
                                STR_TALKING_SECRET5,
                                STR_TALKING_SECRET6,
                                STR_TALKING_SECRET7,
                                STR_TALKING_SECRET8,
                                STR_TALKING_SECRET9,
                                STR_TALKING_SECRET10,
                                STR_TALKING_SECRET11,
                                STR_TALKING_SECRET12,
                                STR_TALKING_SECRET13,
                                STR_TALKING_SECRET14,
                            };
                            int ran = rand()%talk_msg.size();
                            show_centered_msg (talk_msg[ran]);
                        }
                    }
                    s.at(2) = FOUND;
                    break;
                }
            }

            for (unsigned int i = 0; i < zombie.size(); ++i){
                if (! has_eaten (i)) {
                    move_zombie (i);

                    if (has_eaten (i))
                        continue;

                    if (bomb_clock > 0) {
                        if (zombie.at(i)[0] == bomb[0] && zombie.at(i)[1] == bomb[1]) {
                            bomb_clock = 0;
                            kill_zombie (i);
                        }
                    }
                }
            }
            purge_zombies ();

            if (bomb_clock >= 0) {
                bomb_clock--;
                if (hero[0] == bomb[0] && hero[1] == bomb[1]){
                    bomb_clock = 0;
                    set_hero_state (BOMBED);
                }
            }
        }

        void set_bomb ()
        {
            bomb[0] = hero[0];
            bomb[1] = hero[1];
            bomb_clock = max_bomb_clock;
        }

        int get_hero_state ()
        {
            return hero_state;
        }

        void set_game_state (int new_state)
        {
            game_state = new_state;
            if (new_state != GAMING && new_state != LVLUP)
                show_exit_msg ();
        }

        int get_game_state ()
        {
            return game_state;
        }

        void incr_score (int increment)
        {
            set_score (get_score() + increment);
        }

        void set_score (int new_score)
        {
            score = new_score;
        }

        int get_score ()
        {
            return score;
        }

        void set_level (int lvl)
        {
            level = lvl;
        }

        int get_level ()
        {
            return level;
        }

        int get_zombies_count ()
        {
            return zombie_count;
        }

        int get_zombies_left ()
        {
            return zombie.size();
        }

        bool get_play_again ()
        {
            return play_again;
        }

        void refresh_foot_text ()
        {
            string foot_msg = STR_NEED_HELP;

            WINDOW *foot_win;
            foot_win = create_win_borderless (1, foot_msg.length(),
                    COLS-OFFSET-foot_msg.length(), LINES-1);
            mvwprintw (foot_win, 0, 0, foot_msg.c_str());
            wrefresh (foot_win);
            destroy_win (foot_win);
        }

        void refresh_bottom_panel ()
        {
            WINDOW *bpanel_win;

            string score_msg = STR_SCORE + to_string(get_score());
            bpanel_win = create_win (3, score_msg.length()+2,
                    (COLS-score_msg.length()-2)/2, OFFSET+length[1]);
            mvwprintw (bpanel_win, 1, 1, score_msg.c_str());
            wrefresh (bpanel_win);
            destroy_win (bpanel_win);
        }

        void refresh_upper_panel ()
        {
            WINDOW *upanel_win;

            // Threshold to display short version
            const int width_threshold = 30;

            // Left panel: Level
            string level_msg;
            if (COLS > width_threshold)
                level_msg = STR_LEVEL + to_string(get_level());
            else
                level_msg = "N " + to_string(get_level());
            upanel_win = create_win (3, level_msg.length()+2, OFFSET, 1);
            mvwprintw (upanel_win, 1, 1, level_msg.c_str());
            wrefresh (upanel_win);
            destroy_win (upanel_win);

            // Right panel: Zombies left
            string zleft_msg;
            if (COLS > width_threshold)
                zleft_msg = "Zombies: " + to_string(get_zombies_left())
                    + "/" + to_string(get_zombies_count());
            else
                zleft_msg = "Z " + to_string(get_zombies_left())
                    + "/" + to_string(get_zombies_count());
            upanel_win = create_win (3, zleft_msg.length()+2,
                    COLS-OFFSET-zleft_msg.length()-2, 1);
            mvwprintw (upanel_win, 1, 1, zleft_msg.c_str());
            wrefresh (upanel_win);
            destroy_win (upanel_win);
        }

        void show_centered_msg (string msg_text)
        {
            unsigned int msg_h = 3;
            unsigned int msg_w = msg_text.length() + 4;

            // coordinates to center msg
            int msg_y = (LINES-msg_h) / 2;
            int msg_x = (COLS-msg_w) / 2;

            WINDOW *msg_win;
            msg_win = create_win (msg_h, msg_w, msg_x, msg_y);
            mvwprintw (msg_win, 1, 2, msg_text.c_str());
            wrefresh (msg_win);
            while (true) {
                int ch = getch ();
                if (ch != KEY_LEFT && ch != KEY_RIGHT && ch != KEY_DOWN && ch != KEY_UP)
                    break;
            }
            destroy_win (msg_win);
        }

        void show_exit_msg ()
        {
            string exit_msg = get_exit_msg ();
            unsigned int msg_h, msg_w; // msg height and width

            const string question = STR_PLAY_AGAIN;
            msg_w = question.length() + 2;

            bool large_state = (exit_msg.length() > msg_w);
            if (large_state)
                msg_h = exit_msg.length() / msg_w + 5;
            else
                msg_h = 5;

            // coordinates to center msg
            int msg_y = (LINES-msg_h) / 2;
            int msg_x = (COLS-msg_w) / 2;

            WINDOW *msg_win;
            msg_win = create_win (msg_h, msg_w, msg_x, msg_y);
            if (large_state)
                mvwprintw (msg_win, 1, 1, exit_msg.c_str());
            else
                mvwprintw (msg_win, 1, (msg_w-exit_msg.length())/2,
                        exit_msg.c_str());
            mvwprintw (msg_win, 3, 1, question.c_str());
            wrefresh (msg_win);
            while (true) {
                int ch = wgetch (msg_win);
                if (ch == 's' || ch == 'S' || ch == 'y' || ch == 'Y') {
                    play_again = true;
                    break;
                }
                else if (ch == 'n' || ch == 'N') {
                    play_again = false;
                    break;
                }
            }
            destroy_win (msg_win);
        }

        void show_help_msg ()
        {
            const vector<string> help_msg = {
                STR_CONTROL1,
                STR_CONTROL2,
                STR_CONTROL3,
                STR_CONTROL4,
                STR_CONTROL5,
                STR_CONTROL6,
                STR_CONTROL7,
                "",
                STR_HELP1,
                STR_HELP2,
                STR_HELP3,
                STR_HELP4,
                "",
                STR_CONTROL8,
            };

            // msg height and width
            unsigned int msg_h = help_msg.size() + 2;
            unsigned int msg_w = 0;

            // Find the longest line
            for (string line: help_msg)
                if (msg_w < line.length())
                    msg_w = line.length();

            msg_w += 2; // because of the win borders

            // coordinates to center msg
            int msg_y = (LINES-msg_h) / 2;
            int msg_x = (COLS-msg_w) / 2;

            WINDOW *msg_win;
            msg_win = create_win (msg_h, msg_w, msg_x, msg_y);
            for (unsigned int i = 0; i < help_msg.size(); ++i)
                mvwprintw (msg_win, i+1, (msg_w-help_msg[i].length())/2,
                        help_msg[i].c_str());
            wrefresh (msg_win);
            while (true) {
                int ch = wgetch (msg_win);
                if (ch == 'h' || ch == 'H')
                    break;
            }
            destroy_win (msg_win);
        }

        void show_talk_msg ()
        {
            const vector<string> talk_msg = {
                STR_TALK1,
                STR_TALK2,
                STR_TALK3,
                STR_TALK4,
                STR_TALK5,
                STR_TALK6,
                STR_TALK7,
                STR_TALK8,
                STR_TALK9,
            };

            int ran = rand()%talk_msg.size();
            show_centered_msg (talk_msg[ran]);
        }

    private:
        int level;
        int score;
        int length[2];
        int door[3];
        int hero[2];
        char hero_char;
        int zombie_count;
        vector<int> zombie_purge_list;
        vector<vector<int> > zombie;
        int bomb[2];
        int bomb_clock;
        int max_bomb_clock;
        vector<vector<int> > secret;
        WINDOW *map_win;
        int hero_state;
        int game_state;
        bool play_again;

        void init_secret ()
        {
            int secret_count = 0.02 * (length[0]-2) * (length[1]-2);
            secret.resize (secret_count);
            for (auto& s: secret) {
                s.resize (4);
                s.at(0) = rand()%(length[0]-3) + 2;
                s.at(1) = rand()%(length[1]-3) + 2;
                s.at(2) = HIDDEN;
                float ran = rand()*1.0/RAND_MAX;
                if(ran < 0.5)
                    s.at(3) = LASTING_CLOCK;
                else
                    s.at(3) = TALKING_SECRET;
            }
        }

        void init_doors ()
        {
            door[0] = rand()%(length[0]-3) + 2;
            door[1] = 0;
            door[2] = OPEN;
        }

        void set_hero_state (int new_state)
        {
            hero_state = new_state;
            if (new_state != ALIVE)
                set_game_state (LOST);
        }

        void init_zombies ()
        {
            int max_zombies = 0.2 * (length[0]-2) * (length[1]-2);
            zombie_count = level;
            if (zombie_count > max_zombies)
                zombie_count = max_zombies;

            zombie.resize (zombie_count);
            for (auto& z: zombie)
                z.resize (3);

            for (auto z_i = zombie.begin(); z_i != zombie.end(); ++z_i) {
                bool over_hero = true;
                bool over_zombies = true;
                int x, y;
                // Avoid superposition with hero and among zombies
                while (over_hero || over_zombies) {
                    x = rand()%(length[0]-3) + 2;
                    y = rand()%(length[1]-3) + 2;
                    if (x != hero[0] && y != hero[1])
                        over_hero = false;
                    over_zombies = false;
                    for (auto z_j = zombie.begin(); z_j < z_i; ++z_j)
                        if (x == z_j->at(0) && y == z_j->at(1))
                            over_zombies = true;
                }
                z_i->at(0) = x;
                z_i->at(1) = y;
                if (rand()*1.0/RAND_MAX < FOLLOWERS_POP)
                    z_i->at(2) = FOLLOWER;
                else
                    z_i->at(2) = RANDWALK;
            }
        }

        void move_zombie (unsigned int i)
        {
            // TODO: Avoid superposition of zombies
            int zombie_d[2];
            if (zombie.at(i)[2] == RANDWALK) {
                for (int dim = 0; dim < 2; ++dim)
                    zombie_d[dim] = rand()%3 - 1;
            }
            else { // (zombie.at(i)[2] == FOLLOWER)
                for (int dim = 0; dim < 2; ++dim) {
                    if ((hero[dim] - zombie.at(i)[dim]) > 0)
                        zombie_d[dim] = 1;
                    else if ((hero[dim] - zombie.at(i)[dim]) < 0)
                        zombie_d[dim] = -1;
                    else
                        zombie_d[dim] = rand()%3 - 1;
                }
            }
            for (int dim = 0; dim < 2; ++dim)
                zombie.at(i)[dim] += zombie_d[dim];

            // Collision against walls is reversed:
            for (int dim = 0; dim < 2; ++dim) {
                if (zombie.at(i)[dim] == 0)
                    zombie.at(i)[dim] += 2;
                else if (zombie.at(i)[dim] == (length[dim] - 1))
                    zombie.at(i)[dim] -= 2;
            }
        }

        bool has_eaten (unsigned int i)
        {
            if (hero[0] == zombie.at(i)[0] && hero[1] == zombie.at(i)[1]) {
                set_hero_state (EATEN);
                return true;
            }
            return false;
        }

        void kill_zombie (int i)
        {
            zombie_purge_list.push_back (i);
            incr_score (1);
        }

        void purge_zombies ()
        {
            for (int i: zombie_purge_list)
                zombie.erase (zombie.begin()+i);
            zombie_purge_list.clear();

            if (get_zombies_left() == 0)
                set_game_state (LVLUP);
        }

        string get_exit_msg ()
        {
            if (game_state == QUIT)
                return STR_DONTGO_CHAVO;
            else if (game_state == LOST) {
                if (hero_state == EATEN)
                    return STR_EATEN;
                else if (hero_state == BOMBED)
                    return STR_DETONATED;
                else
                    return STR_YOULOSE;
            }
            return STR_WEIRD;
        }
};

#endif /* _WORLD_H */
